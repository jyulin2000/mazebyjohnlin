/**
 * @author John Lin
 */
package gui;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import generation.CardinalDirection;
import generation.Maze;
import gui.Constants.UserInput;
import gui.Robot.Direction;
import gui.Robot.Turn;

public class BasicRobot implements Robot {
	private Controller controller;
	private float batteryLevel; // Keep track of energy of a robot
	public final static float startingBatteryLevel = 3000;
	private int odometer; // Count number of squares traversed
	private boolean hasStopped;
	private boolean[] sensors; // Order: Left, Right, Forward, Backward, all sensors always exist for basic robot.
	private int[] position;

	public static class costs {
		public static final float sense = 1; // Sense for a wall in a direction or check if exit is visible
		public static final float rotate = 3; // 90 degree rotation clockwise or counterclockwise
		public static final float step = 5; // Move forward one step
		public static final float jump = 50; // Jump over a wall to an adjacent cell
	}
	
	public BasicRobot() {
		batteryLevel = startingBatteryLevel;
		odometer = 0;
		hasStopped = false;
		sensors = new boolean[] {true, true, true, true};
	}
	
	@Override
	public int[] getCurrentPosition() throws Exception {
		int[] position = controller.getCurrentPosition();
		return position;
	}

	@Override
	public CardinalDirection getCurrentDirection() {
		return controller.getCurrentDirection();
	}

	@Override
	public void setMaze(Controller controller) {
		this.controller = controller;
		updatePosition();
	}

	@Override
	public float getBatteryLevel() {
		return batteryLevel;
	}

	@Override
	public void setBatteryLevel(float level) {
		batteryLevel = level;
	}

	@Override
	public int getOdometerReading() {
		return odometer;
	}

	@Override
	public void resetOdometer() {
		odometer = 0;
	}

	@Override
	public float getEnergyForFullRotation() {
		return 4 * costs.rotate;
	}
	
	@Override
	public float getEnergyForStepForward() {
		return costs.step;
	}

	@Override
	public boolean isAtExit() {
		Maze maze = controller.getMazeConfiguration();
		
		try {
			position = getCurrentPosition();
		} catch (Exception e) {
		}
		
		return maze.getDistanceToExit(position[0], position[1]) == 1;
	}

	@Override
	public boolean canSeeThroughTheExitIntoEternity(Direction direction) throws UnsupportedOperationException {
		return distanceToObstacle(direction) == Integer.MAX_VALUE;
	}

	@Override
	public boolean isInsideRoom() throws UnsupportedOperationException {
		Maze maze = controller.getMazeConfiguration();
		updatePosition();
		
		return maze.getFloorplan().isInRoom(position[0], position[1]);
	}

	@Override
	public boolean hasRoomSensor() {
		// Always true for basic robot
		return true;
	}

	@Override
	public boolean hasStopped() {
		return hasStopped;
	}

	@Override
	public int distanceToObstacle(Direction direction) throws UnsupportedOperationException {
		if (!hasOperationalSensor(direction)) {
			throw new UnsupportedOperationException();
		}
		
		if (batteryLevel >= costs.sense) {
			batteryLevel -= costs.sense;
			
			Maze maze = controller.getMazeConfiguration();
			updatePosition();
			int[] tracker = position;
			
			CardinalDirection dir = absoluteDirection(direction);
			int[] increment = getDirectionShift(dir);
			
			int count = 0;
			
			while (!maze.hasWall(tracker[0], tracker[1], dir)) {
				position[0] += increment[0];
				position[1] += increment[1];
				
				if (!maze.isValidPosition(position[0], position[1])) {
					return Integer.MAX_VALUE;
				}
				
				count += 1;
			}
			
			return count;
		}
		
		else {
			hasStopped = true;
			return -1;
		}
	}
	
	@Override
	public int distanceToObstacleInDirection(CardinalDirection dir) {
		Direction relDirections[] = {Direction.FORWARD, Direction.LEFT, Direction.BACKWARD, Direction.RIGHT};
		List<Direction> relDirectionArray = Arrays.asList(relDirections);
		Direction side = null;
		
		// Yep I also copy pasted this here from Wizard
		switch (getCurrentDirection()) {
		case North:
			switch (dir) {
			case North:
				side = Direction.FORWARD;
				break;
			case East:
				side = Direction.LEFT;
				break;
			case South:
				side = Direction.BACKWARD;
				break;
			case West:
				side = Direction.RIGHT;
				break;
			}
			break;
		case East:
			switch (dir) {
			case North:
				side = Direction.RIGHT;
				break;
			case East:
				side = Direction.FORWARD;
				break;
			case South:
				side = Direction.LEFT;
				break;
			case West:
				side = Direction.BACKWARD;
				break;
			}
			break;
		case South:
			switch (dir) {
			case North:
				side = Direction.BACKWARD;
				break;
			case East:
				side = Direction.RIGHT;
				break;
			case South:
				side = Direction.FORWARD;
				break;
			case West:
				side = Direction.LEFT;
				break;
			}
			break;
		case West:
			switch (dir) {
			case North:
				side = Direction.LEFT;
				break;
			case East:
				side = Direction.BACKWARD;
				break;
			case South:
				side = Direction.RIGHT;
				break;
			case West:
				side = Direction.FORWARD;
				break;
			}
			break;
		}
		
		if (hasOperationalSensor(side)) {
			return distanceToObstacle(side);
		}
		else {
			while (true) {
				side = relDirections[(relDirectionArray.indexOf(side)+1)%4];
				rotate(Turn.RIGHT);
				if (hasOperationalSensor(side)) {
					return distanceToObstacle(side);
				}
			}
		}
	}
	@Override
	public boolean hasOperationalSensor(Direction direction) {
		return sensors[directionToIndex(direction)];
	}

	@Override
	public void triggerSensorFailure(Direction direction) {
		sensors[directionToIndex(direction)] = false;
	}
	
	@Override
	public boolean repairFailedSensor(Direction direction) {
		sensors[directionToIndex(direction)] = true;
		
		return true;
	}

	@Override
	public void rotate(Turn turn) {
		if (batteryLevel >= costs.rotate) {
			switch (turn) {
				case LEFT:
					controller.keyDown(UserInput.Left, 0);
					batteryLevel -= costs.rotate;
					break;
				case RIGHT:
					controller.keyDown(UserInput.Right, 0);
					batteryLevel -= costs.rotate;
					break;
				case AROUND:
					controller.keyDown(UserInput.Right, 0);
					batteryLevel -= costs.rotate;
					if (batteryLevel >= costs.rotate) {
						controller.keyDown(UserInput.Right, 0); 
						batteryLevel -= costs.rotate;
					}
					else { hasStopped = true; }
			}
		}
		
		else { hasStopped = true; }
	}
	
	@Override
	public void move(int distance, boolean manual) {
		Maze maze = controller.getMazeConfiguration();
		CardinalDirection direction = getCurrentDirection();
		if (distance > 0) {
			int progress = 0;
			
			updatePosition();
			
			while (progress < distance) {
				if (batteryLevel >= costs.step) {
					if (!maze.hasWall(position[0], position[1], direction)) {
						controller.keyDown(UserInput.Up, 0);
						batteryLevel -= costs.step;

						updatePosition();
						
						progress++;
						odometer++;
					}
					else {
						if (manual) {
							break;
						}
						else {
							hasStopped = true;
							break;
						}
					}
				}
				
				else { 
					hasStopped = true;
					break;
				}
			}
		}
	}
	
	@Override
	public void turnTowards(CardinalDirection cardDir) {
		Direction turnDir = null;
		
		for (Direction dir : Direction.values()) {
			if (absoluteDirection(dir) == cardDir) {
				turnDir = dir;
			}
		}
		
		switch (turnDir) {
			case LEFT:
				rotate(Turn.LEFT);
				break;
			case RIGHT:
				rotate(Turn.RIGHT);
				break;
			case BACKWARD:
				rotate(Turn.AROUND);
				break;
			case FORWARD:
				break;
		}
	}
	
	@Override
	public void jump() throws Exception {
		Maze maze = controller.getMazeConfiguration();
		boolean invalidJump = false;

		updatePosition();
		
		switch (getCurrentDirection()) {
			case North:
				if (position[1] == 0) invalidJump = true;
				break;
			case South:
				if (position[1] == maze.getHeight() - 1) invalidJump = true;
				break;
			case East:
				if (position[0] == maze.getWidth() - 1) invalidJump = true;
				break;
			case West:
				if (position[0] == 0) invalidJump = true;
				break;
		}
		
		if (invalidJump) {
			hasStopped = true;
			throw new Exception("Cannot jump outside of the maze");
		}
		
		if (batteryLevel >= costs.jump) {
			controller.keyDown(UserInput.Jump, 0);
			batteryLevel -= costs.jump;
			odometer++;
		}
		else { hasStopped = true; }
	}
	
	private void updatePosition() {
		try {
			position = getCurrentPosition();
		} catch (Exception e) {
		}
	}
	
	private int directionToIndex(Direction direction) {
		switch (direction) {
			case LEFT:
				return 0;
			case RIGHT:
				return 1;
			case FORWARD:
				return 2;
			case BACKWARD:
				return 3;
			default:
				return -1;
		}
	}
	
	/**
	 * Translate a given relative direction to a cardinal direction based on the given
	 * direction and the current direction the robot is facing.
	 * @param direction
	 * @return
	 */
	private CardinalDirection absoluteDirection(Direction direction) {
		CardinalDirection[] dir = {CardinalDirection.North, CardinalDirection.East, CardinalDirection.South, CardinalDirection.West};
		List<CardinalDirection> directions = Arrays.asList(dir);
		
		int cur = directions.indexOf(getCurrentDirection());
		
		switch (direction) {
			case FORWARD:
				return directions.get(cur);
			case LEFT:
				return directions.get((cur + 1) % 4);
			case BACKWARD:
				return directions.get((cur + 2) % 4);
			case RIGHT:
				return directions.get((cur + 3) % 4);
			default:
				return null;
		}
	}
	
	public static int[] getDirectionShift(CardinalDirection direction) {
		switch (direction) {
			case North:
				return new int[] {0, -1};
			case South:
				return new int[] {0, 1};
			case East:
				return new int[] {1, 0};
			case West:
				return new int[] {-1, 0};
			default:
				return null;
		}
	}
}
