/**
 * Robot driver that uses a naive wall following algorithm to search for the exit.
 * The basic idea is for the robot to 'follow' the wall, like a person touching the wall 
 * to their left and keeping their hand on that wall as they move and encounter junctions.
 * 
 * We can accomplish this behavior by choosing actions from a list of precedence,
 * choosing the highest precedence move that is possible in the current position.
 * From each position, attempt to move in the following directions in the following
 * order of precedence: Left, Forward, Right, Backward
 * @author John Lin
 *
 */
package gui;

import java.util.Arrays;
import java.util.List;

import generation.CardinalDirection;
import generation.Distance;
import gui.Robot.Direction;
import gui.Robot.Turn;

public class WallFollower implements RobotDriver {
	private Robot robot;
	private int width;
	private int height;
	private Distance distance;
	private boolean[] sensors; // Order: Left, Right, Forward, Backwards
	
	public WallFollower() {
		sensors = new boolean[] {true, true, true, true};
	}
	@Override
	public void setRobot(Robot r) {
		this.robot = r;
	}

	@Override
	public void setDimensions(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	/**
	 * We don't use this for the wall follower, which is blind to the overall layout.
	 * So we will just do nothing here, sinc we don't need to save the Distance object if given.
	 */
	@Override
	public void setDistance(Distance distance) {
		return;
	}

	@Override
	public void triggerUpdateSensorInformation() {
		sensors[0] = robot.hasOperationalSensor(Direction.LEFT);
		sensors[1] = robot.hasOperationalSensor(Direction.RIGHT);
		sensors[2] = robot.hasOperationalSensor(Direction.FORWARD);
		sensors[3] = robot.hasOperationalSensor(Direction.BACKWARD);
	}
	
	/**
	 * From each position, attempt to move in the following directions in the following
	 * order of precedence: Left, Forward, Right, Backward
	 */
	@Override
	public boolean drive2Exit() throws Exception {
		while (!robot.hasStopped()) {
			CardinalDirection bearing = robot.getCurrentDirection();
			if (robot.isAtExit()) {
				int[] pos = robot.getCurrentPosition();
				if (pos[0] == 0) {
					if (robot.distanceToObstacleInDirection(CardinalDirection.West) == Integer.MAX_VALUE) {
						robot.turnTowards(CardinalDirection.West);
					}
				}
				else if (pos[0] == width - 1) {
					if (robot.distanceToObstacleInDirection(CardinalDirection.East) == Integer.MAX_VALUE) {
						robot.turnTowards(CardinalDirection.East);
					}
				}
				else if (pos[1] == 0) {
					if (robot.distanceToObstacleInDirection(CardinalDirection.North) == Integer.MAX_VALUE) {
						robot.turnTowards(CardinalDirection.North);
					}
				}
				else if (pos[1] == height - 1) {
					if (robot.distanceToObstacleInDirection(CardinalDirection.South) == Integer.MAX_VALUE) {
						robot.turnTowards(CardinalDirection.South);
					}
				}
				robot.move(1, false);
				return true;
			}
			//Try left first
			if (robot.distanceToObstacleInDirection(leftOf(bearing, 1)) > 0) {
				robot.turnTowards(leftOf(bearing, 1));
				robot.move(1, false);
			}
			//Then try forward
			else if (robot.distanceToObstacleInDirection(bearing) > 0) {
				robot.turnTowards(bearing);
				robot.move(1,  false);
			}
			//Then try right
			else if (robot.distanceToObstacleInDirection(leftOf(bearing, 3)) > 0) {
				robot.turnTowards(leftOf(bearing, 3));
				robot.move(1,  false);
			}
			else if (robot.distanceToObstacleInDirection(leftOf(bearing, 2)) > 0) {
				robot.turnTowards(leftOf(bearing, 2));
				robot.move(1, false);
			}
		}
		
		throw new Exception();
	}

	@Override
	public float getEnergyConsumption() {
		return (BasicRobot.startingBatteryLevel - robot.getBatteryLevel());
	}

	@Override
	public int getPathLength() {
		return robot.getOdometerReading();
	}
	
	private CardinalDirection leftOf(CardinalDirection dir, int n) {
		if (n == 0) {
			return dir;
		}
		else {
			switch (dir) {
			case North:
				return leftOf(CardinalDirection.East, n-1);
			case East:
				return leftOf(CardinalDirection.South, n-1);
			case South:
				return leftOf(CardinalDirection.West, n-1);
			case West:
				return leftOf(CardinalDirection.North, n-1);
			default:
				return null;
			}
		}
	}
}