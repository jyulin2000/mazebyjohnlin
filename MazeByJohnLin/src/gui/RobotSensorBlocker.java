/**
 * A runnable class that turns a robot sensor on and off according to a fixed time delay.
 * @author John Lin
 * 
 */
package gui;

import java.sql.Driver;
import java.util.concurrent.atomic.AtomicBoolean;

import gui.Robot.Direction;

public class RobotSensorBlocker implements Runnable {
	public final int delta_t = 3000; // milliseconds
	private AtomicBoolean running = new AtomicBoolean(false);
	private Robot robot;
	private Direction sensor;
	private RobotDriver driver;
	
	public RobotSensorBlocker(Robot robot, RobotDriver driver, Direction sensor) {
		this.robot = robot;
		this.sensor = sensor;
		this.driver = driver;
	}
	
	public void refresh() {
		running.set(true);
	}
	public void interrupt() {
		running.set(false);
	}
	
	public boolean isRunning() {
		return running.get();
	}

	@Override
	public void run() {
		running.set(true);
		
		while (running.get()) {
			try {
				Thread.sleep(delta_t);
				robot.triggerSensorFailure(sensor);
				driver.triggerUpdateSensorInformation();
				Thread.sleep(delta_t);
				robot.repairFailedSensor(sensor);
				driver.triggerUpdateSensorInformation();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				running.set(false);
			}
		}
		return;
	}
}
