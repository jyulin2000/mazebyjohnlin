/**
 * Robot driver that 'cheats' using knowledge of the map layout by using a
 * Distance object to follow the shortest path to the exit.
 * We accomplish this behavior by using the Distance object to find the distance
 * to the exit from the current the position and the distance to the exit from
 * all adjacent squares. If the robot can jump over a wall save on both
 * distance to exit and on energy, then it will do so. Otherwise, it will simply
 * move to the adjacent square that does not have a wall between it and has
 * a distance to the exit that is one less than the current position.
 * @author John Lin
 *
 */
package gui;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import generation.CardinalDirection;
import generation.Distance;
import gui.Robot.Direction;
import gui.Robot.Turn;

public class Wizard implements RobotDriver {
	private Robot robot;
	private int width;
	private int height;
	private Distance distance;
	private boolean[] sensors; // Order: Left, Right, Forward, Backwards
	
	public Wizard() {
		sensors = new boolean[] {true, true, true, true};
	}
	
	@Override
	public void setRobot(Robot r) {
		this.robot = r;
	}

	@Override
	public void setDimensions(int width, int height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public void setDistance(Distance distance) {
		this.distance = distance;
	}

	@Override
	public void triggerUpdateSensorInformation() {
		sensors[0] = robot.hasOperationalSensor(Direction.LEFT);
		sensors[1] = robot.hasOperationalSensor(Direction.RIGHT);
		sensors[2] = robot.hasOperationalSensor(Direction.FORWARD);
		sensors[3] = robot.hasOperationalSensor(Direction.BACKWARD);
	}
	
	/**
	 * From each position, move to the position that is closer to the exit. Jump over walls
	 * if it saves both path length and energy.
	 */
	@Override
	public boolean drive2Exit() throws Exception {		
		HashMap<CardinalDirection, Integer> adjDists = new HashMap<>();
		for (CardinalDirection dir : CardinalDirection.values()) {
			adjDists.put(dir, Integer.MAX_VALUE);
		}
		
		while (!robot.hasStopped()) {
			int[] pos = robot.getCurrentPosition();
			int curDist = distance.getDistanceValue(pos[0], pos[1]);
			
			if (robot.isAtExit()) {
				if (pos[0] == 0) {
					if (robot.distanceToObstacleInDirection(CardinalDirection.West) == Integer.MAX_VALUE) {
						robot.turnTowards(CardinalDirection.West);
					}
				}
				else if (pos[0] == width - 1) {
					if (robot.distanceToObstacleInDirection(CardinalDirection.East) == Integer.MAX_VALUE) {
						robot.turnTowards(CardinalDirection.East);
					}
				}
				else if (pos[1] == 0) {
					if (robot.distanceToObstacleInDirection(CardinalDirection.North) == Integer.MAX_VALUE) {
						robot.turnTowards(CardinalDirection.North);
					}
				}
				else if (pos[1] == height - 1) {
					if (robot.distanceToObstacleInDirection(CardinalDirection.South) == Integer.MAX_VALUE) {
						robot.turnTowards(CardinalDirection.South);
					}
				}
				robot.move(1, false);
				return true;
			}
			
			CardinalDirection moveDir = null;
			for (CardinalDirection dir : CardinalDirection.values()) {
				int[] delta = BasicRobot.getDirectionShift(dir);
				int[] newPos = new int[] {pos[0] + delta[0], pos[1] + delta[1]};
				if (newPos[0] >= 0 && newPos[0] <= width - 1 && newPos[1] >= 0 && newPos[1] <= height -1) {
					int dist = distance.getDistanceValue(newPos[0], newPos[1]);
					
					if (dist == curDist - 1) {
						if (robot.distanceToObstacleInDirection(dir) > 0) {
							moveDir = dir;
						}
					}
					adjDists.replace(dir, dist);
				}
			}
			//Logic for jumping//
			robot.turnTowards(moveDir);
			robot.move(1, false);
		}
		System.out.println("failure");
		throw new Exception();
	}
	
	@Override
	public float getEnergyConsumption() {
		return (BasicRobot.startingBatteryLevel - robot.getBatteryLevel());
	}

	@Override
	public int getPathLength() {
		return robot.getOdometerReading();
	}
}