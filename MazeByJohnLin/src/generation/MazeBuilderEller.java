package generation;

import generation.Wallboard;
import generation.CardinalDirection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class MazeBuilderEller extends MazeBuilder implements Runnable {
	/**
	 * Probability of joining adjacent disjoint cells as a percentage
	 */
	private int joinProbability = 70;
	
	/**
	 * Indicates which set cells belong to
	 */
	private int[][] cellSetRecord;
	
	/**
	 * setList object to handle set operations.
	 */
	private setList list;
	
	/**
	 * Handles the set operations necessary for Eller's algorithm, assigns cells to the proper set
	 * when initialized or when two sets are merged.
	 * @author John Lin
	 *
	 */
	private class setList {
		private LinkedList<Integer> availableSlots;
		private final int initialSize = 20;
		private ArrayList<HashSet<int[]>> list;
		
		public setList() {
			list = new ArrayList<HashSet<int[]>>(initialSize);
			for (int i = 0; i < initialSize; i++) list.add(new HashSet<int[]>());
			for (int[] row: cellSetRecord) Arrays.fill(row, -1);
			
			availableSlots = new LinkedList<Integer>();
			for (int i = 0; i < initialSize; i++) {
				availableSlots.add(i);
			}
			
			populateRooms();
		}
		
		private int nextAvailableSlot() {
			if (availableSlots.size() == 0) {
				list.add(new HashSet<int[]>());
				return list.size() - 1;
			}
			
			else return availableSlots.remove();
		}
		
		/**
		 * Locates and assigns rooms to sets before Eller's algorithm takes place.
		 */
		private void populateRooms() {
			LinkedList<int[][]> foundRooms = new LinkedList<int[][]>();
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					int[] cell = new int[] {x, y};
					if (cellInFoundRoom(cell, foundRooms)) {
						continue;
					}
					else {
						if (floorplan.isInRoom(x, y)) {
							int[] botRight = findBottomRightOfRoom(cell);
							foundRooms.add(new int[][] {cell, botRight});
							populateRoom(cell, botRight);
						}
					}
				}
			}
		}
		
		private void populateRoom(int[] topLeft, int[] botRight) {
			int slot = nextAvailableSlot();
			for (int x = topLeft[0]; x <= botRight[0]; x++) {
				for (int y = topLeft[1]; y <= botRight[1]; y++) {
					cellSetRecord[x][y] = slot;
					list.get(slot).add(new int[] {x, y});
				}
			}
		}
		
		private boolean cellInFoundRoom(int[] cell, LinkedList<int[][]> foundRooms) {
			for (int[][] corners : foundRooms) {
				int[] topLeft = corners[0];
				int[] botRight = corners[1];
				if (cell[0] >= topLeft[0] && cell[0] <= botRight[0] &&
						cell[1] >= topLeft[1] && cell[1] <= botRight[1]) {
					return true;
				}
			}
			
			return false;
		}
		
		private int[] findBottomRightOfRoom(int[] topLeft) {
			int[] cell = new int[] {topLeft[0], topLeft[1]};
			
			// Find right coordinate
			while (cell[0] < width - 1 && floorplan.isInRoom(cell[0] + 1, cell[1])) {
				cell[0] += 1;
			}
			
			// Find bottom coordinate
			while (cell[1] < height - 1 && floorplan.isInRoom(cell[0], cell[1] + 1)) {
				cell[1] += 1;
			}
			
			return cell;
		}
		
		public void addToNewSet(int[] cell) {
			int nextAvailableSlot = nextAvailableSlot();
			list.set(nextAvailableSlot, new HashSet<int[]>());
			list.get(nextAvailableSlot).add(cell);
			cellSetRecord[cell[0]][cell[1]] = nextAvailableSlot;		
		}

		public void mergeSets(int index1, int index2) {
			if (list.get(index1).size() > list.get(index2).size()) {
				list.get(index1).addAll(list.get(index2));
				for (int[] i : list.get(index2)) {
					cellSetRecord[i[0]][i[1]] = index1;
				}
				list.set(index2, new HashSet<int[]>());
				availableSlots.add(index2);
			}
			else {
				list.get(index2).addAll(list.get(index1));
				for (int[] i: list.get(index1)) {
					cellSetRecord[i[0]][i[1]] = index2;
				}
				list.set(index1, new HashSet<int[]>());
				availableSlots.add(index1);
			}
		}
		
		public void addCellToSet(int[] cell, int set) {
			int newSet = cellSetRecord[cell[0]][cell[1]];
			if (newSet < 0) {
				cellSetRecord[cell[0]][cell[1]] = set;
				list.get(set).add(cell);
			}
			else {
				mergeSets(set, newSet);
			}
		}
		
		// Get a random cell in a specified row, in a specified set for the descent part of Eller's algorithm.
		// We expect that such a cell exists when we get this call, or else we return null. This is not very 
		// robust, but this method only has one specific purpose (for the row descent).
		public int[] getRandomCell(int y, int set) {
			List<int[]> candidateCells= new ArrayList<int[]>(width);
			for (int x = 0; x < width; x++) {
				if (cellSetRecord[x][y] == set) {
					candidateCells.add(new int[] {x, y});
				}
			}
			if (candidateCells.size() > 0) {
				return candidateCells.get(random.nextIntWithinInterval(0, candidateCells.size() - 1));
			}
			else return null;
		}
	}
	
	/**
	 * Maze builder for a randomly generated maze generated using Eller's algorithm.
	 */
	public MazeBuilderEller() {
		super();
		System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze.");
	}
	
	/**
	 * Maze builder for a deterministically generated maze generated using Eller's algorithm.
	 * @param det a boolean, true or false.
	 */
	public MazeBuilderEller(boolean det) {
		super(det);
		System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze.");
	}
	
	/**
	 * Generate pathways through an initialized floorplan using Eller's Algorithm.
	 * For each row:
	 * 	1. Assign unexplored cells to a new set.
	 * 	2. Randomly remove wallboards between disjoint cells
	 * 	3. Randomly connect cells to the next row down, ensuring that each set accounted for
	 * 		in the current row has at least one path down.
	 * Then at the bottom row, connect all disjoint cells to ensure that the maze is fully connected
	 */
	@Override
	protected void generatePathways() {		
		cellSetRecord = new int[width][height];
		list = new setList();
		
		//A set of the sets present in the current row
		HashSet<Integer> rowSets = new HashSet<Integer>();

		for (int y = 0; y < height - 1; y++) {
			rowSets = new HashSet<Integer>();
			
			initialAssignRow(y);
			
			randomlyJoinRow(y);
			
			randomlyDescendRow(y);
		}
		
		initialAssignRow(height - 1);
		joinBottomRow();
	}
	
	/**
	 * Assign all unexplored cells in the row to a new set.
	 * @param y the index of the row.
	 */
	private void initialAssignRow(int y) {
		for (int x = 0; x < width; x++) {
			if (cellSetRecord[x][y] < 0) {
				list.addToNewSet(new int[] {x, y});
			}
		}
	}
	
	/**
	 * Randomly join adjacent disjoint cells. If a pair of adjacent disjoint cells is
	 * found, determine whether or not to make a path by calling shouldJoin(), which returns true/false
	 * according to a distribution determined by the field joinProbability.
	 * @param y the index of the row.
	 */
	private void randomlyJoinRow(int y) {
		for (int x = 0; x < width - 1; x++) {
			if (cellSetRecord[x][y] != cellSetRecord[x+1][y] && !floorplan.hasWall(x,  y,  CardinalDirection.East)) {
				list.mergeSets(cellSetRecord[x][y], cellSetRecord[x+1][y]);
			}
			else if (cellSetRecord[x][y] != cellSetRecord[x+1][y] && shouldJoin()) {
				Wallboard wall = new Wallboard(x, y, CardinalDirection.East);
				if (floorplan.hasWall(x, y, CardinalDirection.East)) {
					floorplan.deleteWallboard(wall);
				}
				
				list.mergeSets(cellSetRecord[x][y], cellSetRecord[x+1][y]);
			}
		}
	}
	
	/**
	 * Ensure that every set accounted for in the current row has a path down to the next row.
	 * For each set present in the current row, choose one or more random cells from that set in the row
	 * to connect to the next row down.
	 * @param y the index of the row.
	 */
	private void randomlyDescendRow(int y) {
		for (int x = 0; x < width; x++) {
			if ((cellSetRecord[x][y] != cellSetRecord[x][y+1]) && !(floorplan.hasWall(x, y, CardinalDirection.South))) {
				list.mergeSets(cellSetRecord[x][y], cellSetRecord[x][y+1]);
			}
		}
		for (int i : getSetsInRow(y)) {
			int[] dropCell = list.getRandomCell(y, i);
			
			if (null != dropCell) {
				int X = dropCell[0]; int Y = dropCell[1];
				Wallboard wall = new Wallboard(X, Y, CardinalDirection.South);
				if (floorplan.hasWall(X, Y, CardinalDirection.South)) {
					floorplan.deleteWallboard(wall);
				}
				
				if (cellSetRecord[X][Y+1] < 0) {
					list.addCellToSet(new int[] {X, Y + 1}, cellSetRecord[X][Y]);
				}
				
				else if (cellSetRecord[X][Y] != cellSetRecord[X][Y+1]) {
					list.mergeSets(cellSetRecord[X][Y], cellSetRecord[X][Y+1]);
				}
			}
		}
	}
	
	/**
	 * Return a set denoting every unique set present in the row.
	 * @param y the index of the row.
	 * @return a set of the existing set id's.
	 */
	private Set<Integer> getSetsInRow(int y) {
		Set<Integer> sets = new HashSet<Integer>();
		for (int x = 0; x < width; x++) {
			sets.add(cellSetRecord[x][y]);
		}
		
		return sets;
	}
	
	/**
	 * Join together ALL adjacent disjoint cells to ensure the maze is fully connected.
	 */
	private void joinBottomRow() {
		int y = height - 1;
		for (int x = 0; x < width - 1; x++) {
			if (cellSetRecord[x][y] != cellSetRecord[x+1][y]) {
				Wallboard wall = new Wallboard(x, y, CardinalDirection.East);
				if (floorplan.hasWall(x, y, CardinalDirection.East)) {
					floorplan.deleteWallboard(wall);
				}
				list.mergeSets(cellSetRecord[x][y], cellSetRecord[x+1][y]);
			}
		}
	}
	
	/**
	 * Randomly returns true or false according to a distribution determined by the field joinProbability.
	 * @return a boolean, true or false.
	 */
	private boolean shouldJoin() {
		return random.nextIntWithinInterval(0, 100) <= joinProbability;
	}
}