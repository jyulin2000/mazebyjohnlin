/**
 * Test functionality of BasicRobot class.
 * @Author John Lin
 */

package gui;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;

import generation.CardinalDirection;
import gui.BasicRobot.costs;
import gui.Constants.UserInput;
import gui.Robot.Direction;
import gui.Robot.Turn;

public class BasicRobotTest {
	Controller controller;
	Robot robot;
	
	/**
	 * Use the provided floorplan in input.xml, so we have a deterministic maze we can
	 * test our methods on. The starting position is (4,0), and the starting direction
	 * is East.
	 */
	@Before
	public void setUp() {
		controller = new Controller();
		controller.setFileName("test\\data\\input.xml");
		controller.start();
		
		robot = new BasicRobot();
		robot.setMaze(controller);
	}
	
	/**
	 * Move forward one step from the starting position and starting direction.
	 * This should move the robot from (4,0) to (5,0)
	 */
	@Test
	public final void testMoveForwardOneStep() {
		robot.move(1, false);
		
		try {
			assertEquals(robot.getCurrentPosition()[0], 5);
			assertEquals(robot.getCurrentPosition()[1], 0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Move forward three steps from the starting position and starting direction.
	 * This should move the robot from (4,0) to (7,0).
	 */
	@Test
	public final void testMoveForwardThreeSteps() {
		robot.move(3, false);
		
		try {
			assertEquals(robot.getCurrentPosition()[0], 7);
			assertEquals(robot.getCurrentPosition()[1], 0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Rotate 90 degrees to the left. This should rotate the robot from facing East
	 * to facing South.
	 */
	@Test
	public final void testQuarterTurnLeft() {
		robot.rotate(Turn.LEFT);
		
		assertEquals(CardinalDirection.South, robot.getCurrentDirection());
	}
	
	/**
	 * Rotate 90 degrees to the left. This should rotate the robot from facing East
	 * to facing North.
	 */
	@Test
	public final void testQuarterTurnRight() {
		robot.rotate(Turn.RIGHT);
		
		assertEquals(CardinalDirection.North, robot.getCurrentDirection());
	}
	
	/**
	 * Rotate 180 degrees. This should rotate the robot from facing East to facing West.
	 */
	@Test
	public final void testHalfTurn() {
		robot.rotate(Turn.AROUND);
		
		assertEquals(CardinalDirection.West, robot.getCurrentDirection());
	}
	
	/**
	 * Rotate 360 degrees. Robot should end up in original direction, East.
	 */
	@Test
	public final void testFullTurn() {
		for (int i = 0; i < 4; i++) {
			robot.rotate(Turn.LEFT);
		}
		
		assertEquals(CardinalDirection.East, robot.getCurrentDirection());
	}
	
	/**
	 * Try jumping over a wall into a valid position in the maze. Should end up in
	 * position (4, 1).
	 */
	@Test
	public final void testValidJump() {
		robot.rotate(Turn.LEFT);
		try {
			robot.jump();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		try {
			assertEquals(robot.getCurrentPosition()[0], 4);
			assertEquals(robot.getCurrentPosition()[1], 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Try jumping over a wall into a position outside the maze. Should throw 
	 * an Exception.
	 */
	@Test
	public final void testInvalidJump() {
		robot.rotate(Turn.RIGHT);
		assertThrows(Exception.class, () -> {robot.jump();});
		assertTrue(robot.hasStopped());
	}
	
	/**
	 * Test that trying to move forward when a wall is blocking the way makes the
	 * robot remain in place, and that robot does not fail if manual is true.
	 */
	@Test
	public final void testMoveIntoWallWithManual() {
		robot.rotate(Turn.LEFT);
		
		robot.move(1, true);
		
		try {
			assertEquals(robot.getCurrentPosition()[0], 4);
			assertEquals(robot.getCurrentPosition()[1], 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		assertFalse(robot.hasStopped());
	}
	
	/**
	 * Test that trying to move forward when a wall is blocking the way makes the
	 * robot remain in place, and that robot fails if manual is false.
	 */
	@Test
	public final void testMoveIntoWallWithoutManual() {
		robot.rotate(Turn.LEFT);
		
		robot.move(1, false);
		
		try {
			assertEquals(robot.getCurrentPosition()[0], 4);
			assertEquals(robot.getCurrentPosition()[1], 0);
		} catch (Exception e) {
		}
		
		assertTrue(robot.hasStopped());
	}
	
	/**
	 * Test that the odometer properly records path length, and resets to 0 when
	 * resetOdometer is called.
	 */
	@Test
	public final void testOdometer() {
		robot.move(3, true);
		
		assertEquals(3, robot.getOdometerReading());
		
		robot.resetOdometer();
		
		assertEquals(0, robot.getOdometerReading());
	}
	
	/**
	 * Test that turning and moving consumes the appropriate amount of battery.
	 */
	@Test
	public final void testBatteryConsumption() {
		robot.move(3, true);
		
		assertEquals(2985, Math.round(robot.getBatteryLevel()));
		
		robot.rotate(Turn.AROUND);
		
		assertEquals(2979, Math.round(robot.getBatteryLevel()));
		
		robot.distanceToObstacle(Direction.FORWARD);
		
		assertEquals(2978, Math.round(robot.getBatteryLevel()));
	}
	
	/**
	 * Test that triggering sensor failure properly stops sensor input from
	 * that direction.
	 */
	@Test
	public final void testTriggerSensorFailure() {
		for (Direction dir: Direction.values()) {
			robot.triggerSensorFailure(dir);
		}
		
		for (Direction dir : Direction.values()) {
			assertFalse(robot.hasOperationalSensor(dir));
			assertThrows(UnsupportedOperationException.class, () -> {robot.distanceToObstacle(dir);});
		}
	}
	
	/**
	 * Test that repairing sensor makes it operational again.
	 */
	@Test
	public final void testRepairSensor() {
		for (Direction dir: Direction.values()) {
			robot.triggerSensorFailure(dir);
		}
		
		int failureCount = 0;
		for (Direction dir : Direction.values()) {
			robot.repairFailedSensor(dir);
			try {
				robot.distanceToObstacle(dir);
			} catch (Exception e) {
				failureCount++;
			}
		}
		
		assertEquals(failureCount, 0);
	}
	
	/**
	 * Test that robot returns proper energy costs for operations.
	 */
	@Test
	public final void testEnergyCosts() {
		assertEquals(robot.getEnergyForFullRotation(), 4 * costs.rotate, .01);
		assertEquals(robot.getEnergyForStepForward(), costs.step, .01);
	}
	
	/**
	 * Test that robot has an operational room sensor. This should always be true.
	 */
	@Test
	public final void testHasRoomSensor() {
		assertTrue(robot.hasRoomSensor());
	}
	
	/**
	 * Test that room sensor returns appropriate reading when inside and outside a room.
	 */
	@Test
	public final void testRoomSensor() {
		assertFalse(robot.isInsideRoom());
		
		// Travel to a room
		robot.move(11, true);
		robot.rotate(Turn.LEFT);
		robot.move(2, true);
		robot.rotate(Turn.RIGHT);
		robot.move(1, true);
		
		assertTrue(robot.isInsideRoom());
	}
	
	/**
	 * Test that exit sensor works when at the exit and when not at exit.
	 */
	@Test
	public final void testIsAtExit() {
		assertFalse(robot.isAtExit());
		
		// Travel to exit
		robot.rotate(Turn.AROUND);
		try {
			robot.jump();
		} catch (Exception e) {	}
		robot.move(3, true);
		robot.rotate(Turn.RIGHT);
		robot.move(3,  true);
		
		assertTrue(robot.isAtExit());
	}
	
	/**
	 * Test whether the robot can see the exit in a specified direction.
	 */
	@Test
	public final void testCanSeeThroughExitIntoEternity() {
		assertFalse(robot.canSeeThroughTheExitIntoEternity(Direction.FORWARD));
		
		// Travel to exit
		robot.rotate(Turn.AROUND);
		try {
			robot.jump();
		} catch (Exception e) {	}
		robot.move(3, true);
		robot.rotate(Turn.RIGHT);
		robot.move(3,  true);
		
		assertTrue(robot.canSeeThroughTheExitIntoEternity(Direction.LEFT));
	}
	
	/**
	 * Test that the robot stops when the battery is depleted by a move.
	 */
	@Test
	public final void testBatteryDrainFromMoving() {
		robot.setBatteryLevel(1);
		
		robot.move(1, true);
		assertTrue(robot.hasStopped());
		try {
			assertEquals(robot.getCurrentPosition()[0], 4);
			assertEquals(robot.getCurrentPosition()[1], 0);
		} catch (Exception e) {
		}
	}
	
	/**
	 * Test that the robot stops when the battery is depleted by a turn.
	 */
	@Test
	public final void testBatteryDrainFromTurning() {
		robot.setBatteryLevel(1);
		robot.rotate(Turn.RIGHT);
		assertTrue(robot.hasStopped());
		assertEquals(CardinalDirection.East, robot.getCurrentDirection());
		
		robot.setBatteryLevel(4);
		robot.rotate(Turn.AROUND);
		assertTrue(robot.hasStopped());
		assertEquals(CardinalDirection.North, robot.getCurrentDirection());
	}
	
	/**
	 * Test that the robot stops when the battery is depleted by a sensor.
	 */
	@Test
	public final void testBatteryDrainFromSensing() {
		robot.setBatteryLevel(1);
		robot.distanceToObstacle(Direction.RIGHT);
		robot.distanceToObstacle(Direction.LEFT);
		assertTrue(robot.hasStopped());
	}
}
