/**
 * Test functionality of Wizard class
 * @author John Lin
 */
package gui;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import generation.Order;

public class WizardTest {
	Controller controller;
	Robot robot;
	RobotDriver wizard;
	final int nTrials = 5;
	final int sleepInterval = 25;
	
	/**
	 * Instantiate a maze and add a fresh robot and driver to the controller.
	 */
	@Before
	public void setUp() {
		controller = new Controller();
		robot = new BasicRobot();
		robot.setMaze(controller);
		wizard = new Wizard();
		wizard.setRobot(robot);
		controller.setRobotAndDriver(robot, wizard);
		controller.setBuilder(Order.Builder.DFS);
	}
	
	/**
	 * Ensure Wizard solves a range of mazes. Puts a wizard in a variety of mazes,
	 * then verifies it reaches the exit.
	 */
	@Test
	public void testWizardSolvesMaze() {
		for (int i = 0; i < nTrials; i++) {
			controller.start();
			controller.switchFromTitleToGenerating(0);
			
			while (!(controller.currentState instanceof StateWinning)) {
				try {
					Thread.sleep(sleepInterval);
				} catch (InterruptedException e) {}
			}
			
			assertTrue(controller.currentState instanceof StateWinning);
			setUp();
		}
	}
	
	/**
	 * Ensure that wizard takes the shortest path by checking that it takes
	 * the minimum number of steps to reach the exit.
	 */
	@Test
	public void testWizardTakesShortestPath() {
		for (int i = 0; i < nTrials; i++) {
			controller.start();
			controller.switchFromTitleToGenerating(0);
			
			while (controller.currentState instanceof StateGenerating) {
				try {
					Thread.sleep(sleepInterval);
				} catch (InterruptedException e) {}
			}
			
			int[] start = controller.getMazeConfiguration().getStartingPosition();
			int minPathLength = controller.getMazeConfiguration().getDistanceToExit(start[0], start[1]);
			
			while (!(controller.currentState instanceof StateWinning)) {
				try {
					Thread.sleep(25);
				} catch (InterruptedException e) {}
			}
			
			assertTrue(minPathLength >= robot.getOdometerReading());
			setUp();
		}
	}
	
	/**
	 * Sometimes an adjacent square will have a shorter path length to exit but
	 * be blocked by a wall. Test that wizard does not try to cross this wall unless
	 * it is energy efficient.
	 */
	@Test
	public void testWizardDoesNotRunIntoWalls() {
		for (int i = 0; i < nTrials; i++) {
			controller.start();
			controller.switchFromTitleToGenerating(0);
			
			while (!(controller.currentState instanceof StateWinning)) {
				try {
					Thread.sleep(sleepInterval);
				} catch (InterruptedException e) {}
			}
			
			assertFalse(robot.hasStopped());
			assertTrue(controller.currentState instanceof StateWinning);
			setUp();
		}
	}
}
