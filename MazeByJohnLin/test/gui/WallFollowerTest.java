/**
 * Test functionality of wall follower class.
 * @author John Lin
 */
package gui;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import generation.Order;

public class WallFollowerTest {
	Controller controller;
	Robot robot;
	RobotDriver driver;
	final int nTrials = 5;
	final int sleepInterval = 25;
	
	/**
	 * Instantiate a maze and add a fresh robot and driver to the controller.
	 */
	@Before
	public void setUp() {
		controller = new Controller();
		robot = new BasicRobot();
		robot.setMaze(controller);
		driver = new WallFollower();
		driver.setRobot(robot);
		controller.setRobotAndDriver(robot, driver);
		controller.setBuilder(Order.Builder.DFS);
	}
	
	/**
	 * Ensure Wizard solves a range of mazes. Puts a wizard in a variety of mazes,
	 * then verifies it reaches the exit.
	 */
	@Test
	public void testWallFollowerSolvesMaze() {
		for (int i = 0; i < nTrials; i++) {
			controller.start();
			controller.switchFromTitleToGenerating(0);
			
			while (!(controller.currentState instanceof StateWinning)) {
				try {
					Thread.sleep(sleepInterval);
				} catch (InterruptedException e) {}
			}
			
			assertTrue(controller.currentState instanceof StateWinning);
			setUp();
		}
	}
	
	/**
	 * Verify that the wall follower does not get stuck in the maze or hit a wall.
	 */
	@Test
	public void testWallFollowerDoesNotRunIntoWalls() {
		for (int i = 0; i < nTrials; i++) {
			controller.start();
			controller.switchFromTitleToGenerating(0);
			
			while (!(controller.currentState instanceof StateWinning)) {
				try {
					Thread.sleep(sleepInterval);
				} catch (InterruptedException e) {}
			}
			
			assertFalse(robot.hasStopped());
			assertTrue(controller.currentState instanceof StateWinning);
			setUp();
		}
	}
}
