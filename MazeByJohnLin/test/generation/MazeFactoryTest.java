package generation;
import static org.junit.Assert.*;
import generation.CardinalDirection;
import generation.Floorplan;
import generation.Order;
import generation.Order.Builder;

import org.junit.Before;
import org.junit.Test;

public class MazeFactoryTest {
	protected Maze maze;
	protected Distance dists;
	protected int skillLevel = 4;
	protected Builder builder = Builder.DFS;
	
	public static class orderStub implements Order {
		private Builder builder;
		private Maze mazeConfig;
		private int progress;
		private int skillLevel;
		
		public orderStub(Builder builder, int skillLevel) {
			this.builder = builder;
			this.skillLevel = skillLevel;
		}
		
		@Override
		public int getSkillLevel() {
			return skillLevel;
		}

		@Override
		public Builder getBuilder() {
			return builder;
		}

		@Override
		public boolean isPerfect() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void deliver(Maze mazeConfig) {
			this.mazeConfig = mazeConfig;
		}

		@Override
		public void updateProgress(int percentage) {
			progress = percentage;
		}
		
		public Maze getMaze() {
			return mazeConfig;
		}
		
		public int getProgress() {
			return progress;
		}
		
	}
	
	@Before
	public void setUp() {
		orderStub order = new orderStub(builder, skillLevel);
		Factory factory = new MazeFactory(true);
		
		factory.order(order);
		factory.waitTillDelivered();
		maze = order.getMaze();
		dists = maze.getMazedists();
	}
	
	/**
	 * Verify that every cell is reachable from every other cell (no dead zones), and that
	 * the exit can be reached from every cell.
	 */
	@Test
	public final void testAllCellsReachable() {
		for (int x = 0; x < maze.getWidth(); x++) {
			for (int y = 0; y < maze.getHeight(); y++) {
				assertFalse(Distance.INFINITY == dists.getDistanceValue(x, y));
			}
		}
	}
	
	/**
	 * Verify that one and only one exit from the maze exists.
	 * We do this by checking that there exists one cell with
	 * distance 1 in the distance matrix.
	 */
	@Test
	public final void testOneExit() {
		int numberOfExits = 0;
		int[] exitCell = new int[2];
		for (int x = 0; x < maze.getWidth(); x++) {
			for (int y = 0; y < maze.getHeight(); y++) {
				if (dists.getDistanceValue(x, y) == 1) {
					numberOfExits += 1;
					exitCell[0] = x; exitCell[1] = y;
				}
			}
		}
		assertTrue(1 == numberOfExits);
	}
	
	/**
	 * Verify that the border of the maze has only one missing wallboard.
	 */
	@Test
	public final void testOneMissingWallboard() {
		Floorplan floorplan = maze.getFloorplan();
		int width = maze.getWidth();
		int height = maze.getHeight();
		int missingWallBoardTotal = 0;
		for (int x = 0; x < width; x++) {
			if (!(floorplan.hasWall(x, 0, CardinalDirection.North))) {
				missingWallBoardTotal += 1;
			}
			if (!(floorplan.hasWall(x,  height-1, CardinalDirection.South))) {
				missingWallBoardTotal += 1;
			}
		}
		for (int y = 0; y < height; y++) {
			if (!(floorplan.hasWall(0, y, CardinalDirection.West))) {
				missingWallBoardTotal += 1;
			}
			if (!(floorplan.hasWall(width - 1, y, CardinalDirection.East))) {
				missingWallBoardTotal += 1;
			}
		}
		
		assertTrue(1 == missingWallBoardTotal);
	}
	
	/**
	 * Verify that setting deterministic to true results in identical mazes each time.
	 */
	@Test
	public final void testDeterministicConstructor() {
		orderStub order = new orderStub(builder, skillLevel);
		Factory factory = new MazeFactory(true);
		factory.order(order);
		factory.waitTillDelivered();
		Maze maze2 = order.getMaze();
		assertEquals(maze.getFloorplan(), maze2.getFloorplan());
	}
	
	/**
	 * Verify that the maze factory rejects a given order while already busy with
	 * a different order.
	 */
	@Test
	public final void testSecondOrder() {
		orderStub order = new orderStub(builder, skillLevel);
		orderStub order2 = new orderStub(builder, skillLevel);
		Factory factory = new MazeFactory(true);
		
		factory.order(order);
		assertFalse(factory.order(order2));
		factory.cancel();
	}
	
	/**
	 * Verify that the maze factory runs properly with a range of different skill
	 * level inputs.
	 */
	@Test
	public final void testSkillLevel() {
		Factory factory = new MazeFactory(true);
		
		int skillLevels[] = new int[] {0, 2, 5, 8};
		for (int i : skillLevels) {
			orderStub order = new orderStub(builder, i);
			factory.order(order);
			factory.waitTillDelivered();
			assertTrue(order.getMaze() != null);
		}
	}
}